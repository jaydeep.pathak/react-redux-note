import React, { Component } from "react";
import { connect } from "react-redux";
import { removeNote } from "../redux/actions/actions";

class ListNotes extends Component {
  state = { notes: [{ title: "note-1", content: "ajdnasjdnjnsajndjasn" }] };

  removeNote = index => {
    this.props.removeNote(index);
  };
  render() {
    const { notes } = this.props;
    return (
      <React.Fragment>
        <ul>
          {notes.map((note, index) => (
            <li key={index}>
              <b>{note.title}</b>
              <button onClick={() => this.removeNote(index)}>x</button>
              <br />
              <span>{note.content}</span>
            </li>
          ))}
        </ul>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return { notes: state.notes };
};
const mapDispatchToProps = { removeNote: removeNote };

export default connect(mapStateToProps, mapDispatchToProps)(ListNotes);
