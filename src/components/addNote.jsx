import React, { Component } from "react";
import { connect } from "react-redux";
import { addNote } from "../redux/actions/actions";

class AddNote extends Component {
  state = { title: "", content: "" };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSubmit = e => {
    e.preventDefault();
    // alert("submit");
    const { title, content } = this.state;
    this.props.addNote(title, content);

    this.setState({ title: "", content: "" });
  };
  render() {
    return (
      <React.Fragment>
        <form onSubmit={e => this.handleSubmit(e)}>
          <div>
            <label htmlFor="title">Title : </label>
            <input
              type="text"
              id="title"
              name="title"
              value={this.state.title}
              onChange={e => this.handleChange(e)}
            />
          </div>
          <br />
          <div>
            <label htmlFor="content">Note : </label>
            <textarea
              name="content"
              id="content"
              value={this.state.content}
              onChange={e => this.handleChange(e)}
            ></textarea>
          </div>
          <br />
          <button type="submit">Add Note</button>
        </form>
      </React.Fragment>
    );
  }
}
const mapStateToProps = null;
const mapDispatchToProps = { addNote: addNote };
export default connect(mapStateToProps, mapDispatchToProps)(AddNote);
