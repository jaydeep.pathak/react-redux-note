import React from "react";
import AddNote from "./addNote";
import ListNotes from "./listNotes";

function Notes() {
  return (
    <React.Fragment>
      <AddNote />
      <hr />
      <ListNotes />
    </React.Fragment>
  );
}
export default Notes;
