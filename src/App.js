import React from "react";
import Notes from "./components/notes";
import "./App.css";

function App() {
  return (
    <React.Fragment>
      <Notes />
    </React.Fragment>
  );
}

export default App;
